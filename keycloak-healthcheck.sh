#!/bin/bash

set -eo pipefail
curl -s "$KEYCLOAK_URL/realms/master" | [[ "$(jq 'has("token-service")')" == 'true' ]]
