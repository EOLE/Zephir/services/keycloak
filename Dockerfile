####
#### Temporary layer to prepare installation
####
FROM jboss/keycloak:4.8.3.Final AS build

ARG CONTAINERPILOT_VERSION=3.4.3
ARG CONTAINERPILOT_CHECKSUM=e8258ed166bcb3de3e06638936dcc2cae32c7c58

RUN curl -Lso /tmp/containerpilot.tar.gz \
         "https://github.com/joyent/containerpilot/releases/download/${CONTAINERPILOT_VERSION}/containerpilot-${CONTAINERPILOT_VERSION}.tar.gz" \
    && echo "${CONTAINERPILOT_CHECKSUM}  /tmp/containerpilot.tar.gz" | sha1sum -c \
    && tar zxf /tmp/containerpilot.tar.gz -C /tmp


####
#### Target layer
####
FROM jboss/keycloak:4.8.3.Final

USER root

# Manage container with ContainerPilot
COPY --from=build /tmp/containerpilot /usr/local/bin
COPY containerpilot.json5 /etc/containerpilot.json5

# Service controller
COPY configure-keycloak.sh /configure-keycloak.sh
COPY keycloak-healthcheck.sh /keycloak-healthcheck.sh

RUN chown root /usr/local/bin/containerpilot \
    && chgrp root /usr/local/bin/containerpilot \
    && chmod 755 /usr/local/bin/containerpilot \
    && chmod +x /configure-keycloak.sh \
    && chmod +x /keycloak-healthcheck.sh

# Import CA certificates
COPY ca/TERENASSLCA3.crt /etc/pki/ca-trust/source/anchors/
RUN update-ca-trust
RUN update-ca-trust enable

WORKDIR /opt/jboss
ENTRYPOINT [""]
CMD ["/usr/local/bin/containerpilot", "-config", "/etc/containerpilot.json5"]
