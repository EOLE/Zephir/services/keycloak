die() { echo "$@" 1>&2 ; exit 1; }

function waitUrl()
{
    local URL="$1"
    local NB="${2:-30}"
    #echo "waitUrl: $URL"
    while [ "${NB}" -gt 0 ] ; do
      if curl --output /tmp/curl --silent --head "$URL"
      then
          HTTPCODE=$(awk 'NR==1 { print $2; }' /tmp/curl)
      else
          HTTPCODE="-1"
      fi

      if [ "$HTTPCODE" = "200" ] || [ "$HTTPCODE" = "303" ] #|| [ "$HTTPCODE" = "404" ]
      then
          return 0
      fi
      sleep 1
      NB=$(( NB - 1))
    done
    cat /tmp/curl
    return 1
}

function createRole()
{
    local ROLE_NAME="$1"
    local ROLE_DESC="$2"
    $kcadm create roles -r $realm -s name=$ROLE_NAME -s "$ROLE_DESC"
    [ $? = 0 ] || die "Unable to create '$ROLE_NAME' role"
    echo "Role $ROLE_NAME created."
}

function affectGroupToRole()
{
    local GROUP_ID="$1"
    local ROLE_NAME="$2"
    $kcadm add-roles -r $realm --gid $GROUP_ID --rolename $ROLE_NAME
    [ $? = 0 ] || die "Unable to affect '$GROUP_ID' role to the '$ROLE_NAME' group"
    echo "Group $GROUP_ID affected to $ROLE_NAME"
}

function createGroup()
{
    local GROUP_NAME="$1"
    echo $GROUP_NAME
    GROUP_ID=$($kcadm create groups -r $realm -s name=$GROUP_NAME -i)
    [ $? = 0 ] || die "Unable to create '$GROUP_NAME' group"
    echo "Group $GROUP_NAME created with gid $GROUP_ID"
}

function createUser()
{
    local USERNAME="$1"
    local PASSWORD="$2"
    local GROUP_ID="$3"

    #########################################
    # Create users
    #########################################
    USER_UID=$($kcadm create users -r "$realm" -s username="$USERNAME" -s enabled=true -i)
    [ $? = 0 ] || die "Unable to create '$USERNAME' user"

    $kcadm update users/$USER_UID/reset-password \
      -r $realm \
      -s type=password \
      -s value="$PASSWORD" \
      -s temporary=false \
      -n
    [ $? = 0 ] || die "Unable to set '$USERNAME' password"

    echo "User '$USERNAME' created with UID=$USER_UID"

    if [ -n "$GROUP_ID" ]
    then
        #########################################
        # Group affectations
        #########################################
        $kcadm update users/$USER_UID/groups/$GROUP_ID \
             -r $realm \
             -s realm=$realm \
             -s userId=$USER_UID \
             -s groupId=$GROUP_ID \
             -n
        [ $? = 0 ] || die "Unable to affect '$USER_UID' user to the '$GROUP_ID' group"
        echo "$USERNAME user affected to the '$GROUP_ID' group."
    fi
}


kcadm=$JBOSS_HOME/bin/kcadm.sh
output=/opt/jboss/keycloak
mkdir -p "$output"

realm=$KC_REALM_NAME
[ -z "$realm" ] && die "Realm not set. Beware to call this script with Make!"

#########################################
# Login
#########################################

waitUrl "$KEYCLOAK_URL"

# see : http://www.keycloak.org/docs/3.1/server_admin/topics/admin-cli.html
$kcadm config credentials --server $KEYCLOAK_URL --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD
[ $? = 0 ] || die "Unable to login"
$kcadm get realms/$realm 1> /dev/null
if [ $? = 0 ]
then
  echo "Realm '$realm' already exists. Abort configuration."
  exit 0
fi

#########################################
# Create realm
#########################################
REALM_ID=$($kcadm create realms -s realm=$realm -s enabled=true -i)
[ $? = 0 ] || die "Unable to create realm"
echo "Realm '$REALM_ID' created."

$kcadm update realms/$realm -s registrationAllowed=true -s rememberMe=true -s revokeRefreshToken=true -s internationalizationEnabled=true -s defaultLocale="fr"
[ $? = 0 ] || die "Unable to configure realm"
echo "Realm '$REALM_ID' configured."

echo "clean ouput keys..."
rm $output/{*.pem,*.json} 2> /dev/null
[ $? = 0 ] && echo "Output directory cleaned!"

echo "Get realm keys..."
$kcadm get keys -r $realm >$output/keys.json
if [ $? = 0 ]
then
   echo "Unable to get realm keys ... Let's retry in 10s"
   sleep 10
   $kcadm get keys -r $realm >$output/keys.json
   [ $? = 0 ] || die "Unable to get realm keys"
fi
cat $output/keys.json

jq '.keys[] | select(has("publicKey")) | .publicKey ' -r $output/keys.json > $output/pub.tmp
sed -e "1 i -----BEGIN PUBLIC KEY-----" -e "$ a -----END PUBLIC KEY-----" $output/pub.tmp > $output/pub.pem
rm $output/pub.tmp
jq '.keys[] | select(has("certificate")) | .certificate' -r $output/keys.json > $output/cert.tmp
sed -e "1 i -----BEGIN CERTIFICATE-----" -e "$ a -----END CERTIFICATE-----" $output/cert.tmp > $output/cert.pem
rm $output/cert.tmp

#########################################
# Create roles
#########################################
createRole "root" "description=SuperUser"
createRole "admin" "description=Regular admin with full set of permissions"
createRole "manager" "description=Regular manager with basic set of permissions"
createRole "viewer" "description=Regular technician with only enrollement permissions"

#########################################
# Create groups
#########################################
GROUP_ID=""
createGroup "root"
gidRoot=$GROUP_ID
createGroup "admin"
gidAdmin=$GROUP_ID
createGroup "manager"
gidOperator=$GROUP_ID
createGroup "viewer"
gidInstaller=$GROUP_ID

#########################################
# Role affectation
#########################################
affectGroupToRole "$gidRoot" "root"
affectGroupToRole "$gidAdmin" "admin"
affectGroupToRole "$gidOperator" "manager"
affectGroupToRole "$gidInstaller" "viewer"

#########################################
# Create users
#########################################
USER_UID=""
createUser "$KC_REALM_USERNAME" "$KC_REALM_PASSWORD" "$gidRoot"

#########################################
# Set Default Role
#########################################
defaultRole="viewer"
$kcadm update realms/$realm -s defaultRoles+=$defaultRole

#########################################
# Create client(s)
#########################################

client_id=$($kcadm create clients \
  -r $realm \
  -s clientId=$KC_API_CLIENT_ID \
  -s baseUrl=$SSO_END_POINT \
  -s "redirectUris=[\"$SSO_END_POINT/*\",\"$SSO_END_POINT/*\"]" \
  -s "webOrigins=[\"*\"]" \
  -s publicClient=true \
  -s directAccessGrantsEnabled=true \
  -i)
[ $? = 0 ] || die "Unable to create client"

echo "Client '$client_id' created."

#########################################
# Change Authentification browser flows
#########################################
echo "Update Authentification browser Flows..."
$kcadm get authentication/flows/browser/executions -r $realm > /tmp/tmp.tok
cat /tmp/tmp.tok | jq '.[0]' > /tmp/tmp.tmp.tok
cat /tmp/tmp.tmp.tok | jq '.requirement="DISABLED"' > /tmp/tmp.tok
$kcadm update authentication/flows/browser/executions -f /tmp/tmp.tok -r $realm -o

#########################################
# Getting adapter configuration file
#########################################
echo "Get adapter configuration file..."
$kcadm get clients/$client_id/installation/providers/keycloak-oidc-keycloak-json \
  -r $realm \
  | jq ".[\"auth-server-url\"]=\"$SSO_END_POINT/auth\"" \
  > $output/keycloak.json
[ $? = 0 ] || die "Unable to get configuration file"
cat $output/keycloak.json
cp $output/keycloak.json /tmp/zephir

echo "Keycloak successfully configured."
